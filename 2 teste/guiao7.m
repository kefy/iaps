%Guiao 7

x = load('mistura1.txt');
fa = 22050;
bandaBaseECG = 500;
fmax = 3000;
fp = 4000;

fa = 1/Ta;
N = length(x);
Dur_x = duracaoDoSinal(N,ta);

%Representa��o do sinal no dom�nio do Tempo
n = (0:N-1)';
figure
subplot(2,2,1),stem(n,x),title('Dominio Do Tempo'),xlabel('n'),ylabel('x(n)');axis([0 N -2 2]);
%Representa��o do sinal no dom�nio da frequencia
Xf = fft(x);
Mod_x = abs(Xf);
df = fa/N;
f = (0:df:fa-df);
subplot(2,2,2),stem(f,Mod_x),title('Domino Da Frequencia'),xlabel('f(hz)'),ylabel('Xf'),axis([0 fa 0 2500]);

%Obter ECG
Xf_ecg = Xf;
% Desmodular o sinal
Xf_ecg(f>500 & f < 21550) = 0; % coloca os valores entre estes valores a 0. Sao os valores que correspondem � m�sica
x_ecg = ifft(Xf_ecg);
figure,stem(n*Ta,x_ecg),xlabel('t(s)'),ylabel('x(t)');
[Tb,Ab] = ginput(2);
Tb = mean(diff(Tb));
BPM = 60/Tb;

% Determina��o da fase da portadora
fase_portadora = FasePortadora(Xf,fp,f);

%3. Obten��o do ficheiro de audio
    %Desmodula��o do sinal
       Xf_audio_mod = Xf;
       Xf_audio_mod(f<500 | f > 21550) = 0;
       x_audio_mod = ifft(Xf_audio_mod);
       figure, stem(n,x_audio_mod);xlabel('n'),ylabel('x audio mod(n)');

        
    % Filtragem passa-baixo
        z = x_audio_mod.*cos(2*pi*fp/fa*n+fase_portadora*pi/180);  %% aqui o sinal fica desmodulado
        Zf = fft(z);
        Mod_Zf = abs(Zf);
        figure
        stem(f,Mod_Zf,'r.'), xlabel('f(Hz)'),ylabel('Zf')
        Zf(f>3000 & f < (fa-3000))=0; % Passa Baixo
        zdesm = ifft(Zf);
        sound(zdesm,20050);

 %4 Desmodula��o usando outros angulos

    for i=1:length(phi);
        z = x_audio_mod.*cos(2*pi*fp/fa*n+phi(i));
        Zf = fft(z);Mod_Zf = abs(Zf);
        figure
        stem(f,Mod_Zf,'r.'), xlabel('f(Hz)'),ylabel('Zf')
        Zf(f>3000 & f < (fa-3000))=0;
        x_audio_desmod(:,i) = ifft(Zf);
        sound(x_audio_desmod(:,i),22050);
    end;

        
