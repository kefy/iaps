% Guiao 6

% Exercicio 1
DualTone = [ 0  941 1336;
             1  697 1209;
             2  697 1336;
             3  697 1477;
             4  770 1209;
             5  770 1339;
             6  770 1477;
             7  852 1209;
             8  852 1336;
             9  85  1477    ];
         

fa = 8000;
ta = 1/fa
duracao = 0.1;
N_digito = duracao*fa;          % numero de amostras de cada d�gito
separacao = 0.05;
N_separacao =  separacao*fa;
amplitude = 0.5


% Exercicio 1.a

sequencia = [ 1 5 7 3];
n = 0:(N_digito-1);

Cod_dig = zeros(length(sequencia),N_digito); % devolve matriz length(sequencia) * N_digito com zeros
Cod_sep = zeros(1,N_separacao);
Cod_seq = [];

for ii = 1:length(SEQ);

    I = find(DualTone(:,1) == sequencia(ii));
    Cod_dig(ii,:) = 0.5*sin(2*pi*DualTone(I,2)/fa*n)+0.5*sin(2*pi*DualTone(I,3)/fa*n);
    Cod_seq = [Cod_seq Cod_dig(ii,:) Cod_sep];
    
    %Codifica��o de cada d�gito
  %  figure
   % stem(n,COD_dig(ii,:),'k'),title(sprintf('D�gito %d',ii));
   % axis([0 100 -1 1]);
end;



% Exercicio 1.b
    %Adicao de ruido [-0.5 0.5]
    noise = repmat([-0.5 0.5],1,length(Cod_seq)/2);
    newCod_seq =  noise + Cod_seq;
    snrNoise = SNR(energiaDoSinal(newCod_seq,ta),energiaDoSinal(noise,ta));

    
% Exercicio 1.c
    stem(Cod_seq);
    stem(newCod_seq);
    
    checkFrequency(newCod_seq,fa,2);
    
    % fa min de 1450 mas com o sinal com ruido fa min tem que ser maior de
    % forma a evitar perda de informa��o. fa ao ser maior a resolu��o ser�
    % maior tamb�m.
    
% Exercicio 2.

load('dtmf1.txt');
fa = 8000;
ta = 1/fa;
N = length(dtmf1)
duracao = duracaoDoSinal(N,ta);
t = 0:ta:(duracao - ta); %% distribui�ao do sinal
plot(t,dtmf1);
[X Y] = ginput(2);
DuracaoDigito = abs(X(1)-X(2)); % cerca de 100 ms
[X Y] = ginput(2);
DuracaoSeparacao = abs(X(1)-X(2)); %% aprocimadamente 50 ms

stem(dtmf1);
checkFrequency(dtmf1,fa,2);
checkDigits(dtmf1,fa,1)





    
