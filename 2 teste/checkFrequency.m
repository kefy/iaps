function [ X Y ] = checkFrequency(x, fa, inputs )
% fun��o para verificar as frequencias associadas.

df = fa/length(x);
f = (0:df:fa-df);
y = fft(x);
Mod_y = abs(y);
Arg_y = angle(y);
figure
subplot(2,1,1),stem(f,Mod_y,'r.'),xlabel('f(Hz)'),ylabel('Mod_y');
subplot(2,1,2),stem(f,Arg_y,'r.'),xlabel('f(hz)'),ylabel('Arg_y');

if (inputs ~= 0)
[ X Y ] = ginput(inputs);
else
    X = 0;
    Y = 0;
end;

end

