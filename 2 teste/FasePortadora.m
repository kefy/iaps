function fase_portadora = FasePortadora( Xf,fp,f)
%FASEPORTADORA Identificação da fase portadora Entrada Xf obtido por fft e
%filtrado, fp = modelação em amplitude, f = eixo do x em Frequencia
%   

figure
Arg_Xf = angle(Xf);
stem(f,Arg_Xf,'r.'),xlabel('(Hz)'), ylabel('Arg_Xf'), axis([0 8000 -4 4])
I = find(f == fp); % verificar a zona onde o sinal sofreu uma modelação em amplitude
fase_portadora = Arg_Xf(I)*180/pi;


end

