
% Guiao 5

% PARTE 1

%Exercicio 1
f1 = 15;
f2 = 40;
amostras = 1000;
duracao  = 2;
n  = 0:amostras-1;
ta = duracao / amostras;
fa = 1/ta;

x1 = cos(2*pi*f1*n/fa);
x2 = cos(2*pi*f2*n/fa);
x  = x1 + x2;

[X Y ] = checkFrequency(x,fa,1)

% PARTE II

ecgNoise = load('ecgNoise.txt');
ecg1 = load('ecg1.txt');
dur = length(ecgNoise)/fa;
fa = 250;
[ X Y ] = checkFrequency(x,250,0);
[ X Y ] = checkFrequency(ecg1,250,0);

% Exercicio 1 - a
    [ X Y ] = checkFrequency(ecgNoise,250,1); 
    signalSum = mean(ecgNoise) * length(ecgNoise);
    %Resposta: A primeira risca representa o valor m�dio do sinal.
    
% Exercicio 1 - b
    y = fft(ecgNoise);
    y(1) = 0;
    newEcgNoise = ifft(y);
    figure
    subplot(2,1,1),stem(ecgNoise);
    subplot(2,1,2),stem(newEcgNoise);
    % Resposta: Houve uma melhor distribui��o do sinal
    
% Exercicio 1 - c)
[ X Y ] = checkFrequency(ecgNoise,250,2);
[ X Y ] = checkFrequency(ecg1,250,0);
        % Resposta: Olhando para o gr�fico existem 2 componentes de
        % distor��o do sinal
% Exercicio 1 - d)
[ X Y ] = checkFrequency(ecgNoise,250,2);
        % Resposta: Frequencias de 15 e 16 hz
        
% Exercicio 1 - f)
%-------------------------------------------------------
%%%%SINAL DISTORCIDO E SINAL FILTRADO SNR %%%%%%%%%%%
%-------------------------------------------------------
 % Exercicio 2
 load('ecg1.txt');
 
 %Exercicio 2.a)
 figure
 ta = 1/250;
 amostras = length(ecg1);
 n  = 0:amostras - 1;
 t = ta .* n;
 plot(t,ecg1);
 [X Y] = ginput(2);
 periodo = abs(X(1)-X(2));
 bpm = 60/periodo;
    % Resposta: 75 bpm com um periodo proximadamente 0.8 s
  
 % Exercicio 2.b)
 [ X Y ] = checkFrequency(ecg1,fa,1);
    % Resposta : Largura de bando do sinal � aproximadamente 45 hz porque a partir dai
    % um m�dulo alpha de k muito pequeno.

 % Exercicio 2.c)
    % Resposta: Uma vez que a largura de banda � de 45 Hz ent�o esta
    % poderia ser a frequencia de amostgem se satisfazesse fa = 2fmax
    % o que implicaria que a fa minima � de 90 Hz
    
 
